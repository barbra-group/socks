package socks

import (
	"errors"
	"regexp"
	"strings"
)

type nodeType int

const (
	static     nodeType = 0
	regexParam nodeType = 1
	param      nodeType = 2
	catchAll   nodeType = 3
)

func parseNodes(patterns []string, service *Service) ([]*node, error) {
	nodes := []*node{}

	for _, p := range patterns {
		n, err := newNode(p)
		if err != nil {
			return nil, err
		}

		nodes = append(nodes, n)
	}

	nodes[len(nodes)-1].service = service
	return nodes, nil
}

func newNode(pattern string) (*node, error) {
	if strings.HasPrefix(pattern, ":") {
		return newParamNode(pattern)
	}

	if pattern == "*" {
		return nil, errors.New("'*' routes are currently not supported")
	}

	return newStaticNode(pattern), nil
}

func newParamNode(pattern string) (*node, error) {
	r := regexp.MustCompile("^:([^{\n\r]*)(?:{(.+)})?$")
	g := r.FindStringSubmatch(pattern)

	if g[0] == "" {
		return nil, ErrInvalidParamFormat
	}

	if g[2] == "" {
		return &node{
			nType: param,
			pName: g[1],
		}, nil
	}

	re, err := regexp.Compile(g[1])
	if err != nil {
		return nil, err
	}

	return &node{
		nType:  regexParam,
		regExp: re,
		pName:  g[0],
	}, nil
}

func newCatchAllNode() *node {
	return &node{
		nType: catchAll,
	}
}

func newStaticNode(pattern string) *node {
	return &node{
		nType:   static,
		pattern: pattern,
	}
}

func newRootNode() *node {
	return &node{
		nType:   static,
		pattern: "",
	}
}

type node struct {
	children []*node
	pattern  string
	pName    string
	nType    nodeType
	regExp   *regexp.Regexp
	service  *Service
}

func (n *node) AddRoute(path string, service *Service) error {
	patterns := strings.Split(path, "/")
	if sn, _, _ := n.findChildNode(patterns, nil, nil); sn != nil {
		return ErrRouteAlreadyRegistered
	}

	nodes, err := parseNodes(patterns, service)
	if err != nil {
		return err
	}

	n.insertRec(nodes)
	return nil
}

func (n *node) Route(ctx *Context) (*Service, error) {
	n, pn, p := n.findChildNode(strings.Split(ctx.GetResource(), "/"), nil, nil)
	if n == nil {
		return nil, ErrRouteNotFound
	}

	for i := range pn {
		ctx.SetParam(pn[i], p[i])
	}

	return n.service, nil
}

func (n *node) FindService(route string) (*Service, error) {
	sn, _, _ := n.findChildNode(strings.Split(route, "/"), nil, nil)
	if n == nil {
		return nil, ErrRouteNotFound
	}

	return sn.service, nil
}

func (n *node) findNodeRec(patterns, paramNames, params []string) (*node, []string, []string) {
	if n.nType == catchAll {
		return n, paramNames, params
	}

	if n.matchPattern(patterns) {
		if len(patterns) > 1 {
			return n.findChildNode(patterns[1:], paramNames, params)
		}

		return n, paramNames, params
	}

	if n.matchParam(patterns) {
		if len(patterns) > 1 {
			return n.findChildNode(patterns[1:], append(paramNames, n.pName), append(params, patterns[0]))
		}

		return n, append(paramNames, n.pName), append(params, patterns[0])
	}

	return nil, nil, nil
}

func (n *node) findChildNode(patterns, paramNames, params []string) (*node, []string, []string) {
	for _, c := range n.children {
		n, pn, p := c.findNodeRec(patterns, paramNames, params)
		if n != nil {
			return n, pn, p
		}
	}

	return nil, nil, nil
}

func (n *node) matchParam(patterns []string) bool {
	return len(patterns) > 0 && n.nType == param && len(patterns[0]) > 0 || n.nType == regexParam && n.regExp.MatchString(patterns[0])
}

func (n *node) matchPattern(patterns []string) bool {
	return len(patterns) > 0 && n.nType == static && n.pattern == patterns[0]
}

func (n *node) insertRec(nodes []*node) {
	for _, c := range n.children {
		if c == nodes[0] {
			if len(nodes) > 1 {
				n.insertRec(nodes[1:])
				return
			}

			c.service = nodes[0].service
			return
		}
	}

	if len(nodes) > 1 {
		nodes[0].insertRec(nodes[1:])
		n.children = append(n.children, nodes[0])
		return
	}

	n.children = append(n.children, nodes[0])
}
