package socks

func newContext(msg *OperationMsg, client *Client, engine *Engine) *Context {
	return &Context{
		client:  client,
		engine:  engine,
		request: msg,
		params:  make(map[string]string),
	}
}

type Context struct {
	params  map[string]string
	values  map[string]interface{}
	request *OperationMsg
	client  *Client
	engine  *Engine
}

func (ctx *Context) GetParam(key string) (string, error) {
	param, ok := ctx.params[key]
	if !ok {
		return "", ErrParamNotSet
	}

	return param, nil
}

func (ctx *Context) SetParam(key, value string) {
	ctx.params[key] = value
}

func (ctx *Context) Get(key string) (interface{}, error) {
	param, ok := ctx.values[key]
	if !ok {
		return "", ErrValueNotSet
	}

	return param, nil
}

func (ctx *Context) Set(key string, value interface{}) {
	ctx.values[key] = value
}

func (ctx *Context) GetResource() string {
	return ctx.request.Resource
}

func (ctx *Context) SetResource(resource string) {
	ctx.request.Resource = resource
}

func (ctx *Context) GetRequestID() string {
	return ctx.request.RequestID
}

func (ctx *Context) GetOperation() string {
	return ctx.request.Operation
}

func (ctx *Context) Bind(v interface{}) error {
	return ctx.request.Bind(v)
}

func (ctx *Context) Send(msg Message) error {
	return ctx.client.Write(msg)
}

func (ctx *Context) Respond(data interface{}) error {
	return ctx.client.Write(NewOperationMsg(ctx.GetOperation(), ctx.GetResource(), ctx.GetRequestID(), data))
}

func (ctx *Context) Error(err error) error {
	return ctx.Send(NewErrorMsg(ctx.GetResource(), ctx.GetRequestID(), err))
}

func (ctx *Context) CloseConnection() error {
	return ctx.client.Close()
}

// GetUserID returns the id of the user the socket belongs to
func (ctx *Context) GetUserID() string {
	return ctx.client.user
}
