package socks

import (
	"sync"
)

func newService(engine *Engine) *Service {
	service := &Service{
		lock:        &sync.RWMutex{},
		engine:      engine,
		handlers:    make(map[string]Handler),
		subscribers: make(map[string]map[string]*Client),
	}

	service.subPermHandler = func(ctx *Context) error { return nil }
	service.subHandler = func(ctx *Context) error { return service.Subscribe(ctx, ctx.GetResource()) }
	service.unsubHandler = func(ctx *Context) error { return service.Unsubscribe(ctx, ctx.GetResource()) }
	service.requestInterceptor = func(ctx *Context) error { return nil }
	return service
}

type Service struct {
	lock               *sync.RWMutex
	engine             *Engine
	handlers           map[string]Handler
	subscribers        map[string]map[string]*Client
	subPermHandler     Handler
	subHandler         Handler
	unsubHandler       Handler
	requestInterceptor Handler
}

func (s *Service) SetSubscriptionPermissionHandler(handler Handler) {
	s.subPermHandler = handler
}

func (s *Service) SetRequestInterceptor(handler Handler) {
	s.requestInterceptor = handler
}

func (s *Service) MustHandle(operation string, handler Handler) {
	if err := s.Handle(operation, handler); err != nil {
		panic(err)
	}
}

func (s *Service) Handle(operation string, handler Handler, middlewares ...Middleware) error {
	if operation == subOperation || operation == unsubOperation {
		return ErrOperationAlreadyRegistered
	}

	if _, ok := s.handlers[operation]; ok {
		return ErrOperationAlreadyRegistered
	}

	for i := len(middlewares) - 1; i >= 0; i-- {
		handler = middlewares[i](handler)
	}

	s.handlers[operation] = handler
	return nil
}

func (s *Service) Broadcast(event, resource string, data interface{}) {
	s.lock.RLock()
	defer s.lock.RUnlock()

	msg := NewEventMsg(event, resource, data)
	for _, c := range s.subscribers[resource] {
		_ = c.Write(msg)
	}
}

func (s *Service) BroadcastAll(event, resource string, data interface{}) {
	s.lock.RLock()
	defer s.lock.RUnlock()

	msg := NewEventMsg(event, resource, data)
	for _, subs := range s.subscribers {
		for _, c := range subs {
			_ = c.Write(msg)
		}
	}
}

func (s *Service) Subscribe(ctx *Context, resource string) error {
	return s.subscribe(ctx.client, resource)
}

func (s *Service) Unsubscribe(ctx *Context, resource string) error {
	return s.unsubscribe(ctx.client, resource)
}

func (s *Service) subscribe(c *Client, resource string) error {
	s.lock.Lock()
	defer s.lock.Unlock()

	if _, ok := s.subscribers[resource]; !ok {
		s.subscribers[resource] = map[string]*Client{c.GetID(): c}
		return nil
	}

	if _, ok := s.subscribers[resource][c.GetID()]; ok {
		return ErrAlreadySubscribed
	}

	c.addSubscription(resource)
	s.subscribers[resource][c.GetID()] = c
	return nil
}

func (s *Service) unsubscribe(c *Client, resource string) error {
	s.lock.Lock()
	defer s.lock.Unlock()

	if _, ok := s.subscribers[resource]; !ok {
		return ErrNotSubscribed
	}

	if _, ok := s.subscribers[resource][c.GetID()]; !ok {
		return ErrNotSubscribed
	}

	c.removeSubscription(resource)
	if len(s.subscribers[resource]) > 1 {
		delete(s.subscribers[resource], c.GetID())
		return nil
	}

	delete(s.subscribers, resource)
	return nil
}

func (s *Service) handleMessage(ctx *Context) error {
	if err := s.requestInterceptor(ctx); err != nil {
		return err
	}

	if ctx.GetOperation() == subOperation {
		if err := s.subPermHandler(ctx); err != nil {
			return err
		}

		return s.subHandler(ctx)
	}

	if ctx.GetOperation() == unsubOperation {
		return s.unsubHandler(ctx)
	}

	if h, ok := s.handlers[ctx.GetOperation()]; ok {
		return h(ctx)
	}

	return s.engine.operationNotAllowedHandler(ctx)
}
