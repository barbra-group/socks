package socks

import (
	"encoding/json"
)

const (
	subOperation   = "sub"
	unsubOperation = "unsub"
	errOperation   = "error"
)

type Message interface {
	Serialize() ([]byte, error)
}

func deserializeOperationMsg(d []byte) (*OperationMsg, error) {
	msg := &OperationMsg{
		Data: &json.RawMessage{},
	}

	return msg, json.Unmarshal(d, msg)
}

func NewErrorMsg(res, reqID string, err error) *OperationMsg {
	return &OperationMsg{
		Resource:  res,
		RequestID: reqID,
		Operation: errOperation,
		Data: map[string]string{
			"msg": err.Error(),
		},
	}
}

func NewOperationMsg(op, res, reqID string, data interface{}) *OperationMsg {
	return &OperationMsg{
		Resource:  res,
		RequestID: reqID,
		Operation: op,
		Data:      data,
	}
}

type OperationMsg struct {
	Resource  string      `json:"res"           binding:"required"`
	Operation string      `json:"op"            binding:"required"`
	RequestID string      `json:"req,omitempty"`
	Data      interface{} `json:"data,omitempty"`
}

func (msg OperationMsg) Bind(v interface{}) error {
	d, ok := msg.Data.(*json.RawMessage)
	if !ok {
		return ErrDataNotJSON
	}

	return json.Unmarshal(*d, v)
}

func (msg OperationMsg) Serialize() ([]byte, error) {
	return json.Marshal(msg)
}

func NewEventMsg(event, resource string, data interface{}) *EventMsg {
	return &EventMsg{
		Event:    event,
		Resource: resource,
		Data:     data,
	}
}

type EventMsg struct {
	Event    string      `json:"ev"            binding:"required"`
	Resource string      `json:"res"           binding:"required"`
	Data     interface{} `json:"data,omitempty"`
}

func (msg EventMsg) Serialize() ([]byte, error) {
	return json.Marshal(msg)
}
