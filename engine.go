package socks

import (
	"errors"
	"net/http"

	"gopkg.in/go-playground/validator.v9"
)

type Handler func(ctx *Context) error

type Middleware func(Handler) Handler

func New() *Engine {
	e := &Engine{
		validate:                   validator.New(),
		routingTree:                newRootNode(),
		clientInterceptor:          func(*Context) error { return nil },
		resourceNotFoundHandler:    func(*Context) error { return errors.New("Resource not found") },
		operationNotAllowedHandler: func(*Context) error { return errors.New("Operation not allowed") },
	}

	e.hub = newHub(e)
	return e
}

type Engine struct {
	routingTree                *node
	hub                        *hub
	validate                   *validator.Validate
	clientInterceptor          Handler
	resourceNotFoundHandler    Handler
	operationNotAllowedHandler Handler
}

func (e *Engine) MustCreateService(route string) *Service {
	service, err := e.CreateService(route)
	if err != nil {
		panic(err)
	}

	return service
}

func (e *Engine) CreateService(route string) (*Service, error) {
	service := newService(e)
	return service, e.routingTree.AddRoute(route, service)
}

func (e *Engine) HandleRequest(w http.ResponseWriter, r *http.Request) (*Client, error) {
	return e.hub.UpgradeConnection(w, r)
}

func (e *Engine) SetClientInterceptor(handler Handler) {
	e.clientInterceptor = handler
}

func (e *Engine) SetResourceNotFoundHandler(handler Handler) {
	e.resourceNotFoundHandler = handler
}

func (e *Engine) SetOperationNotAllowedHandler(handler Handler) {
	e.operationNotAllowedHandler = handler
}

func (e *Engine) GetClient(id string) (*Client, error) {
	return e.hub.Get(id)
}

func (e *Engine) handleMessage(c *Client, data []byte) {
	msg, err := deserializeOperationMsg(data)
	if err != nil {
		c.Close()
		return
	}

	ctx := newContext(msg, c, e)
	if err := e.clientInterceptor(ctx); err != nil {
		e.mustSendError(ctx, err)
		return
	}

	s, err := e.routingTree.Route(ctx)
	if err != nil {
		e.mustSendError(ctx, err)
		return
	}

	err = s.handleMessage(ctx)
	if err != nil {
		e.mustSendError(ctx, err)
	}
}

func (e *Engine) mustSendError(ctx *Context, err error) {
	if err := ctx.Send(NewErrorMsg(ctx.GetResource(), ctx.GetRequestID(), err)); err != nil {
		ctx.CloseConnection()
	}
}

func (e *Engine) mustUnsubscribeAll(c *Client) {
	for r := range c.subs {
		s, err := e.routingTree.FindService(r)
		if err != nil {
			s.unsubscribe(c, r)
		}
	}
}
