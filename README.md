![Socks- A Barbra Framework](//i.imgur.com/lXRGJ8Mr.png)
## About

Socks is a lightweight websocket framework for the next generation of real-time webapps.

## Services

Services are the core of every socks application. Each service handles one, or a set of, specific resources defined by the route the service is handling. A route can only be handled by one service and can't be registered twice.

Services handle subscriptions, publish events and listen to operations.

Here is an example of a simple service 
```go
func RegisterService(e *socks.Engine) {
    service := &Service{
	    Service: e.MustCreateService("test/service"),
    }

    service.MustHandle("test", service.Test)
}

type Service struct {
	*socks.Service
}

func (s *Service) Test(c *socks.Context) error {
    // Bind data
    data := &map[string]interface{}
    if err := ctx.Bind(data); err != nil {
        return err
    }

    // Broadcast test event
    service.Broadcast("test_event", ctx.GetResource(), "Yay !")

    // Respond data
    return c.Respond("it works !")
}
```

## Routing

All incoming requests are handled by a radix routing tree which always favours the most specific node.

Priority | Type           | Usage             | Example
---------|----------------|-------------------|--------------------------------
1        | Static         | nodeValue         | user/me/profile
2        | Regex-Param    | :paramName{regex} | user/:id{[0-9].}/profile
3        | Param          | :paramName        | user/:id/profile

#### Example

Let's say there a 3 services registered
Service 1 is listening to `user/me/profile`, service 2 to `user/:id{[0-9].}/profile` and service 3 to `user/:id/profile`

When requesting `user/me/profile` service 1 will handle the request since it is has the most specific route that matches the resource. `user/0324/profile` would be handled by service 2 and `user/test/profile` by service 3.

#### Retrieving params

Route params can be retrieved fairly easy using `Context.GetParam("paramName")`

## Protocol

All messages transferred are **JSON** encoded strings and consists of the following 2 types:

#### Event

Field | Type   | Required           | Full Name | Description
------|--------|--------------------|-----------|------------
res   | String | :heavy_check_mark: | Resource  | Resource associated with the Message
ev    | String | :heavy_check_mark: | Event     | Event type
data  | *      | :x:                | Data      | Data associated with the Event

#### Operation

Field | Type   | Required           | Full Name | Description
------|--------|--------------------|-----------|------------
res   | String | :heavy_check_mark: | Resource  | Resource associated with the Message
op    | String | :heavy_check_mark: | Operation | Operation associated with the Message
req   | String | :x:                | RequestID | A client generated string to identify messages
data  | *      | :x:                | Data      | Data associated with the Message

##### Special Operations *("op" field values)*

Value | Send by | Meaning
------|---------|-------
sub   | Client  | Subscribes the current client to all events emitted the given resource
unsub | Client  | Unsubscribes the client from the given resource
err   | Server  | An error occurred (specified in the data field)

## Contributors

This framework was developed an is maintained by the [Barbra](https://barbra.io/) product team. Any contributions are welcomed :sweat_smile:. 