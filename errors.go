package socks

// Error represents an router error.
type Error string

// Error returns the error message.
func (err Error) Error() string {
	return string(err)
}

const (
	ErrClientNotFound = Error("Client not found")
	ErrConnectionClosed = Error("Connection is closed")
	ErrRouteNotFound = Error("Route not found")
	ErrInvalidParamFormat = Error("Invalid param format")
	ErrRouteAlreadyRegistered = Error("Route already registered")
	ErrOperationAlreadyRegistered = Error("Operation handler already registered")
	ErrParamNotSet = Error("Param isn't set")
	ErrValueNotSet = Error("Value isn't set")
	ErrDataNotJSON = Error("Data isn't json")
	ErrAlreadySubscribed = Error("Already subscribed")
	ErrNotSubscribed = Error("Not subscribed")
)
