package socks

import (
	"log"
	"sync"
	"time"

	"github.com/rs/xid"

	"github.com/gorilla/websocket"
)

const (
	maxWriteWait   = 10 * time.Second
	maxPongWait    = 60 * time.Second
	maxPingPeriod  = (maxPongWait * 9) / 10
	maxMessageSize = 512
)

func connectClient(hub *hub, conn *websocket.Conn) *Client {
	client := &Client{
		subs: make(map[string]bool),
		hub:  hub,
		conn: conn,
		send: make(chan []byte),
		open: true,
		lock: &sync.RWMutex{},
		id:   xid.New().String(),
	}

	hub.Register(client)

	go client.readPump()
	go client.writePump()

	return client
}

type Client struct {
	lock *sync.RWMutex
	subs map[string]bool
	user string
	id   string
	hub  *hub
	conn *websocket.Conn
	send chan []byte
	open bool
}

func (c *Client) readPump() {
	defer c.conn.Close()

	c.conn.SetReadLimit(maxMessageSize)
	//c.conn.SetReadDeadline(time.Now().Add(maxPongWait))
	//c.conn.SetPingHandler(c.handlePing)

	for {
		_, data, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("websocket closed abnormaly: %v", err)
			}
			break
		}

		c.hub.handleMessage(c, data)
	}
}

func (c *Client) writePump() {
	for {
		data := <-c.send
		err := c.conn.SetWriteDeadline(time.Now().Add(maxWriteWait))

		if err != nil {
			break
		}

		err = c.conn.WriteMessage(websocket.TextMessage, data)
		if err != nil {
			break
		}
	}
}

func (c *Client) handlePing(message string) error {
	c.conn.SetReadDeadline(time.Now().Add(maxPongWait))
	return nil
}

func (c *Client) WriteRaw(data []byte) error {
	if c.IsClosed() {
		return ErrConnectionClosed
	}

	c.send <- data
	return nil
}

func (c *Client) Write(msg Message) error {
	d, err := msg.Serialize()
	if err != nil {
		return err
	}

	return c.WriteRaw(d)
}

func (c *Client) Close() error {
	if c.IsClosed() {
		return ErrConnectionClosed
	}

	c.lock.Lock()
	c.open = false
	c.conn.Close()
	close(c.send)
	c.lock.Unlock()

	c.hub.Remove(c)
	return nil
}

func (c *Client) addSubscription(resource string) {
	c.lock.Lock()
	c.subs[resource] = true
	c.lock.Unlock()
}

func (c *Client) removeSubscription(resource string) {
	c.lock.Lock()
	delete(c.subs, resource)
	c.lock.Unlock()
}

func (c *Client) IsClosed() bool {
	return !c.open
}

func (c *Client) GetID() string {
	return c.id
}

func (c *Client) GetUser() string {
	return c.user
}

func (c *Client) SetUser(user string) {
	c.user = user
}
