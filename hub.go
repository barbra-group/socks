package socks

import (
	"net/http"
	"sync"

	"github.com/gorilla/websocket"
)

type hub struct {
	lock     *sync.RWMutex
	clients  map[string]*Client
	engine   *Engine
	upgrader websocket.Upgrader
}

func newHub(e *Engine) *hub {
	return &hub{
		lock:    &sync.RWMutex{},
		clients: make(map[string]*Client),
		engine:  e,
		upgrader: websocket.Upgrader{
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
			CheckOrigin: func(r *http.Request) bool {
				return true
			},
		},
	}
}

func (h *hub) UpgradeConnection(w http.ResponseWriter, r *http.Request) (*Client, error) {
	con, err := h.upgrader.Upgrade(w, r, nil)
	if err != nil {
		return nil, err
	}

	return connectClient(h, con), nil
}

func (h *hub) Remove(c *Client) {
	h.lock.Lock()
	delete(h.clients, c.GetID())
	h.lock.Unlock()

	h.engine.mustUnsubscribeAll(c)
}

func (h *hub) Register(c *Client) {
	h.lock.Lock()
	h.clients[c.GetID()] = c
	h.lock.Unlock()
}

func (h *hub) Get(id string) (*Client, error) {
	h.lock.RLock()
	defer h.lock.RUnlock()

	if c, ok := h.clients[id]; ok {
		return c, nil
	}

	return nil, ErrClientNotFound
}

func (h *hub) handleMessage(c *Client, data []byte) {
	h.engine.handleMessage(c, data)
}
